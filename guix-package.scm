(define-module (schemeato)
  #:use-module ((guix licenses)
                #:select (asl2.0)) ; Choose license
  ; add dependency
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (chibi-scheme)
  )


(define-public schemeato
  (let ((commit-ref "6d72a6aff3086abd67d146e12fd1f85cb5dee5ac"))
    (package
      (name "schemeato")
      (version "freeze")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://gitlab.com/ElenQ/schemeato.git")
                       (commit commit-ref)))
                (sha256
                  (base32 "1x3z3lgsx2p35jhkv9ds2lv6rq3s8ky4x6qzs4pqlnlki9pw41bc"))))
      (build-system gnu-build-system)
      (arguments
        `(#:tests? #f
          #:phases (modify-phases
                     %standard-phases
                     (replace 'install
                              (lambda _
                                (copy-recursively "." (string-append %output "/bin"))))
                     (delete 'build)
                     (delete 'configure))))
      
      (home-page "https://gitlab.com/ElenQ/schemeato/")
      (synopsis "Simple static site generator with translation capabilities.")
      (description
        "Schemeato is a static site generator that handles website translations
        in a pretty simple way. It's a second generation of schiumato[^1]
        written in Chibi-Scheme only using standard library. No extra
        dependencies needed.
        Templates are pure scheme with SXML support.
        It uses pandoc command line tool as an optional tool for Markdown
        support.
        [^1]: https://www.npmjs.com/package/schiumato")
        (license asl2.0))))

schemeato
