# Schemeato

Schemeato is a static site generator that handles website translations in a
pretty simple way. It's a second generation of schiumato[^1] written in
Chibi-Scheme only using standard library. No extra dependencies needed.

Templates are pure scheme with SXML support.

It uses pandoc command line tool as an optional tool for Markdown support.

In the examples directory you can find an example of how to use it.

[^1]: https://www.npmjs.com/package/schiumato

