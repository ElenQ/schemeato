(define (template number lang content _)
  (sxml->xml
  `(html
     ((head
      (link (@ (rel "stylesheet")
               (href "/static/style.css")))
      (title ,(string-append (_ "Why Scheme - ") (number->string number))))
    (body
      ,content

      (div (@ (class "controls"))
           (a (@ (href ,(string-append "/" lang "/"
                                       (number->string (- number 1)) ".html"))
                 (class "previous"))
              ,(_ "<- Previous"))
           (a (@ (href ,(string-append "/" lang "/"
                                       (number->string (+ 1 number)) ".html"))
                 (class "next"))
              ,(_ "Next ->"))))))))
