# Example

Run `schemeato create` in this folder and check what happened.

It should create a `www` directory with a copy of the `static` files and all
the templates rendered and translated to each language.
